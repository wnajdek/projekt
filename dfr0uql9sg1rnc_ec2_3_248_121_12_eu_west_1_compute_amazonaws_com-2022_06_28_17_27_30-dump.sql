--
-- PostgreSQL database dump
--

-- Dumped from database version 14.4 (Ubuntu 14.4-1.pgdg20.04+1)
-- Dumped by pg_dump version 14.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: dfr0uql9sg1rnc; Type: DATABASE; Schema: -; Owner: bvckilovivwoay
--

CREATE DATABASE dfr0uql9sg1rnc WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.UTF-8';


ALTER DATABASE dfr0uql9sg1rnc OWNER TO bvckilovivwoay;

\connect dfr0uql9sg1rnc

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: bvckilovivwoay
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO bvckilovivwoay;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: bvckilovivwoay
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: address; Type: TABLE; Schema: public; Owner: bvckilovivwoay
--

CREATE TABLE public.address (
    id integer NOT NULL,
    main_address character varying(255) DEFAULT ''::character varying NOT NULL,
    location_details character varying(255) DEFAULT ''::character varying,
    city character varying(255) DEFAULT ''::character varying NOT NULL,
    postal_code character varying(5) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.address OWNER TO bvckilovivwoay;

--
-- Name: address_id_seq; Type: SEQUENCE; Schema: public; Owner: bvckilovivwoay
--

CREATE SEQUENCE public.address_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.address_id_seq OWNER TO bvckilovivwoay;

--
-- Name: address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bvckilovivwoay
--

ALTER SEQUENCE public.address_id_seq OWNED BY public.address.id;


--
-- Name: product; Type: TABLE; Schema: public; Owner: bvckilovivwoay
--

CREATE TABLE public.product (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    description text,
    price numeric(12,2),
    image character varying(255) NOT NULL,
    product_type_id integer NOT NULL,
    stall_id integer NOT NULL
);


ALTER TABLE public.product OWNER TO bvckilovivwoay;

--
-- Name: product_id_seq; Type: SEQUENCE; Schema: public; Owner: bvckilovivwoay
--

CREATE SEQUENCE public.product_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_id_seq OWNER TO bvckilovivwoay;

--
-- Name: product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bvckilovivwoay
--

ALTER SEQUENCE public.product_id_seq OWNED BY public.product.id;


--
-- Name: product_type; Type: TABLE; Schema: public; Owner: bvckilovivwoay
--

CREATE TABLE public.product_type (
    id integer NOT NULL,
    type character varying(255) NOT NULL
);


ALTER TABLE public.product_type OWNER TO bvckilovivwoay;

--
-- Name: product_type_id_seq; Type: SEQUENCE; Schema: public; Owner: bvckilovivwoay
--

CREATE SEQUENCE public.product_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_type_id_seq OWNER TO bvckilovivwoay;

--
-- Name: product_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bvckilovivwoay
--

ALTER SEQUENCE public.product_type_id_seq OWNED BY public.product_type.id;


--
-- Name: stall; Type: TABLE; Schema: public; Owner: bvckilovivwoay
--

CREATE TABLE public.stall (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    likes integer DEFAULT 0 NOT NULL,
    views integer DEFAULT 0 NOT NULL,
    description text DEFAULT ''::text NOT NULL,
    user_id integer NOT NULL,
    image character varying(255) DEFAULT 'default.jpg'::character varying NOT NULL,
    created_at date DEFAULT CURRENT_DATE NOT NULL,
    is_public boolean DEFAULT false NOT NULL
);


ALTER TABLE public.stall OWNER TO bvckilovivwoay;

--
-- Name: public_stalls; Type: VIEW; Schema: public; Owner: bvckilovivwoay
--

CREATE VIEW public.public_stalls AS
 SELECT stall.id,
    stall.name,
    stall.likes,
    stall.views,
    stall.description,
    stall.user_id,
    stall.image,
    stall.created_at,
    stall.is_public
   FROM public.stall
  WHERE stall.is_public;


ALTER TABLE public.public_stalls OWNER TO bvckilovivwoay;

--
-- Name: role; Type: TABLE; Schema: public; Owner: bvckilovivwoay
--

CREATE TABLE public.role (
    id integer NOT NULL,
    role character varying(100) NOT NULL
);


ALTER TABLE public.role OWNER TO bvckilovivwoay;

--
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: bvckilovivwoay
--

CREATE SEQUENCE public.role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_id_seq OWNER TO bvckilovivwoay;

--
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bvckilovivwoay
--

ALTER SEQUENCE public.role_id_seq OWNED BY public.role.id;


--
-- Name: stall_id_seq; Type: SEQUENCE; Schema: public; Owner: bvckilovivwoay
--

CREATE SEQUENCE public.stall_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stall_id_seq OWNER TO bvckilovivwoay;

--
-- Name: stall_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bvckilovivwoay
--

ALTER SEQUENCE public.stall_id_seq OWNED BY public.stall.id;


--
-- Name: stall_type; Type: TABLE; Schema: public; Owner: bvckilovivwoay
--

CREATE TABLE public.stall_type (
    id integer NOT NULL,
    type character varying(255) NOT NULL
);


ALTER TABLE public.stall_type OWNER TO bvckilovivwoay;

--
-- Name: stall_type_id_seq; Type: SEQUENCE; Schema: public; Owner: bvckilovivwoay
--

CREATE SEQUENCE public.stall_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stall_type_id_seq OWNER TO bvckilovivwoay;

--
-- Name: stall_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bvckilovivwoay
--

ALTER SEQUENCE public.stall_type_id_seq OWNED BY public.stall_type.id;


--
-- Name: stall_types_stall; Type: TABLE; Schema: public; Owner: bvckilovivwoay
--

CREATE TABLE public.stall_types_stall (
    stall_id integer NOT NULL,
    stall_type_id integer NOT NULL
);


ALTER TABLE public.stall_types_stall OWNER TO bvckilovivwoay;

--
-- Name: user; Type: TABLE; Schema: public; Owner: bvckilovivwoay
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    enabled boolean NOT NULL,
    created_at date DEFAULT CURRENT_DATE NOT NULL,
    user_details_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE public."user" OWNER TO bvckilovivwoay;

--
-- Name: user_details; Type: TABLE; Schema: public; Owner: bvckilovivwoay
--

CREATE TABLE public.user_details (
    id integer NOT NULL,
    first_name character varying(100) NOT NULL,
    last_name character varying(100) NOT NULL,
    phone_number character varying(15),
    address_id integer,
    image character varying(255)
);


ALTER TABLE public.user_details OWNER TO bvckilovivwoay;

--
-- Name: user_details_id_seq; Type: SEQUENCE; Schema: public; Owner: bvckilovivwoay
--

CREATE SEQUENCE public.user_details_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_details_id_seq OWNER TO bvckilovivwoay;

--
-- Name: user_details_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bvckilovivwoay
--

ALTER SEQUENCE public.user_details_id_seq OWNED BY public.user_details.id;


--
-- Name: user_favourite_stalls; Type: TABLE; Schema: public; Owner: bvckilovivwoay
--

CREATE TABLE public.user_favourite_stalls (
    id integer NOT NULL,
    user_id integer NOT NULL,
    stall_id integer NOT NULL
);


ALTER TABLE public.user_favourite_stalls OWNER TO bvckilovivwoay;

--
-- Name: user_favourite_stalls_id_seq; Type: SEQUENCE; Schema: public; Owner: bvckilovivwoay
--

CREATE SEQUENCE public.user_favourite_stalls_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_favourite_stalls_id_seq OWNER TO bvckilovivwoay;

--
-- Name: user_favourite_stalls_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bvckilovivwoay
--

ALTER SEQUENCE public.user_favourite_stalls_id_seq OWNED BY public.user_favourite_stalls.id;


--
-- Name: user_user_id_seq; Type: SEQUENCE; Schema: public; Owner: bvckilovivwoay
--

CREATE SEQUENCE public.user_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_user_id_seq OWNER TO bvckilovivwoay;

--
-- Name: user_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bvckilovivwoay
--

ALTER SEQUENCE public.user_user_id_seq OWNED BY public."user".id;


--
-- Name: address id; Type: DEFAULT; Schema: public; Owner: bvckilovivwoay
--

ALTER TABLE ONLY public.address ALTER COLUMN id SET DEFAULT nextval('public.address_id_seq'::regclass);


--
-- Name: product id; Type: DEFAULT; Schema: public; Owner: bvckilovivwoay
--

ALTER TABLE ONLY public.product ALTER COLUMN id SET DEFAULT nextval('public.product_id_seq'::regclass);


--
-- Name: product_type id; Type: DEFAULT; Schema: public; Owner: bvckilovivwoay
--

ALTER TABLE ONLY public.product_type ALTER COLUMN id SET DEFAULT nextval('public.product_type_id_seq'::regclass);


--
-- Name: role id; Type: DEFAULT; Schema: public; Owner: bvckilovivwoay
--

ALTER TABLE ONLY public.role ALTER COLUMN id SET DEFAULT nextval('public.role_id_seq'::regclass);


--
-- Name: stall id; Type: DEFAULT; Schema: public; Owner: bvckilovivwoay
--

ALTER TABLE ONLY public.stall ALTER COLUMN id SET DEFAULT nextval('public.stall_id_seq'::regclass);


--
-- Name: stall_type id; Type: DEFAULT; Schema: public; Owner: bvckilovivwoay
--

ALTER TABLE ONLY public.stall_type ALTER COLUMN id SET DEFAULT nextval('public.stall_type_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: public; Owner: bvckilovivwoay
--

ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_user_id_seq'::regclass);


--
-- Name: user_details id; Type: DEFAULT; Schema: public; Owner: bvckilovivwoay
--

ALTER TABLE ONLY public.user_details ALTER COLUMN id SET DEFAULT nextval('public.user_details_id_seq'::regclass);


--
-- Name: user_favourite_stalls id; Type: DEFAULT; Schema: public; Owner: bvckilovivwoay
--

ALTER TABLE ONLY public.user_favourite_stalls ALTER COLUMN id SET DEFAULT nextval('public.user_favourite_stalls_id_seq'::regclass);


--
-- Data for Name: address; Type: TABLE DATA; Schema: public; Owner: bvckilovivwoay
--

COPY public.address (id, main_address, location_details, city, postal_code) FROM stdin;
1	1234 Blane Street	\N	Saint Louis	63101
34	Chałupy 10		Chałupy	12345
69	None		None	None
70	Chałupy 10		Chałupy	12345
71	Chałupy 10		Chałupy	12345
72	Chałupy 10		Chałupy	12345
66	Aleja Świętego Jakuba		Inowrocław	34211
73	Sosnowa 4		Gdynia	37231
74	Sosnowa 4		Gdynia	37231
75	Leśna 4		Grudziądz	80654
76	Urocza 32		Zabrze	17669
77	Słowackiego 1		Koszalin	84450
78	Porzeczkowa 2		Grudziądz	65743
79	Jarzębinowa 21		Gdynia	38375
80	Dworcowa 3		Częstochowa	78242
81	Leśna		Piekary Śląskie	11306
\.


--
-- Data for Name: product; Type: TABLE DATA; Schema: public; Owner: bvckilovivwoay
--

COPY public.product (id, name, description, price, image, product_type_id, stall_id) FROM stdin;
94	Naleśniki	Nalesniki	12.00	pancakes.jpg	1	13
95	Ziemniaki	Przygotowane ziemniaki	2.00	fries.jpg	1	13
96	Ser kozi	Ser kozi z polskich kóz. Cena za 1kg	20.00	ser-kozi.jpg	1	18
97	Pierogi	Pyszne pierogi w okazyjnej cenie. Cena za 10 sztuk.	10.00	pierogi-z-kapusta.jpg	1	18
98	Kozie mleko	Pyszne mleko. Cena za litr	7.00	mlekokozie.jpg	1	18
99	Maliny	Świeże maliny zbierane wczoraj. Cena za koszyczek.	12.00	maliny.jpg	1	19
100	Borówki	Cena za litr	7.00	borówki.jpg	1	19
101	Agrest	Piękne, wyrośnięte agresty. Cena za litr	6.00	agrest.jpg	1	19
102	Rzodkiewka w pęczkach	Cena za jeden pęczek	5.00	rzodkiewka.jpg	1	23
103	Sałata rzymska	Cena za sztukę	5.00	salata-rzymska.jpg	1	23
104	Sałata pekińska	Cena za sztukę	6.00	pekinska.jpg	1	23
105	Ptaszki z drewna	Własnoręcznie robione ptaszki z drewna jesionowego wysokiej jakości. Cena za sztukę.	10.00	drewniany-malowany-ptaszek.jpg	1	20
50	Naleśniki	najlepsze naleśniki	12.00	pancakes.jpg	1	12
51	Naleśniki	jeszcze lepsza receptura, zero konserwantów	13.00	pancakes.jpg	1	12
53	Eggs	High quality eggs	10.00	eggs.jpg	1	14
54	Eggs	High quality eggs	10.00	eggs.jpg	1	14
56	Naleśniki	Naleśniki	12.00	pancakes.jpg	1	14
55	Frytki	Fries super	5.00	fries.jpg	1	14
57	Frytki	fryty nowe ulepszone	1.00	fries.jpg	1	14
88	Naleśniki kiki	dqwdqwdqw	123.00	pancakes.jpg	1	14
87	Naleśniki	Poprawny opis dla naleśników	123.00	pancakes.jpg	1	14
47	Frytki Extra	nowe frytki zmieniona receptura, teraz jeszcze lepszy smak	7.00	fries.jpg	1	12
89	Eggs	Jajka	3.00	eggs.jpg	1	12
\.


--
-- Data for Name: product_type; Type: TABLE DATA; Schema: public; Owner: bvckilovivwoay
--

COPY public.product_type (id, type) FROM stdin;
1	diary
\.


--
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: bvckilovivwoay
--

COPY public.role (id, role) FROM stdin;
1	USER
2	ADMIN
\.


--
-- Data for Name: stall; Type: TABLE DATA; Schema: public; Owner: bvckilovivwoay
--

COPY public.stall (id, name, likes, views, description, user_id, image, created_at, is_public) FROM stdin;
13	qwerty123's bazaar	2	0	Here is a place for more info about your bazaar	56	rickandmorty.jpg	2022-06-25	t
12	My bazaar	5	0	Here is my new info about my stall.\nHere you can find a lot of good stuff in pretty low price range.\nNew products every Friday!	55	default.jpg	2022-06-20	t
18	iwona1's bazaar	0	0	Here is a place for more info about your bazaar	63	default.jpg	2022-06-28	t
21	aga21's bazaar	0	0	Here is a place for more info about your bazaar	66	default.jpg	2022-06-28	f
22	piotrek.k's bazaar	0	0	Here is a place for more info about your bazaar	67	default.jpg	2022-06-28	f
24	adamski99's bazaar	0	0	Here is a place for more info about your bazaar	69	default.jpg	2022-06-28	f
25	eleo02's bazaar	0	0	Here is a place for more info about your bazaar	70	default.jpg	2022-06-28	f
19	nieznajoma8's bazaar	0	0	Super bazar z super cenami	64	default.jpg	2022-06-28	t
23	Najlepszy bazar	0	0	Tutaj kupisz najtaniej	68	tlo_bazar.jpg	2022-06-28	t
20	wojtek123's bazaar	0	0	Here is a place for more info about your bazaar	65	ptaszki-tlo.jpg	2022-06-28	t
14	Test stall	100	0	This bazaar is for admin testing purposes	59	default.jpg	2022-06-26	f
\.


--
-- Data for Name: stall_type; Type: TABLE DATA; Schema: public; Owner: bvckilovivwoay
--

COPY public.stall_type (id, type) FROM stdin;
1	groceries
3	clothes
2	jewellery
4	other
\.


--
-- Data for Name: stall_types_stall; Type: TABLE DATA; Schema: public; Owner: bvckilovivwoay
--

COPY public.stall_types_stall (stall_id, stall_type_id) FROM stdin;
14	2
12	1
12	3
12	4
13	1
13	3
23	1
20	2
20	4
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: bvckilovivwoay
--

COPY public."user" (id, email, password, enabled, created_at, user_details_id, role_id) FROM stdin;
55	qwerty123wwwww@gmail.com	$2y$10$I4klQTqKFWUBc4wOeTtqieIahHlKZlBRaoZRLdjYINFDUjL3mC29W	t	2022-06-20	49	1
59	scaredtobehungry@gmail.com	$2a$12$WqRRturaP31Dv4.3UjvrguwP5wOhX0gXOf0bEjCxNExQW6U5EfZWW	t	2022-06-26	61	2
56	qwerty123@gmail.com	$2y$10$4MXGA0yVdxXa2sBkLWcoxutLRKDN2TUOpW6XMhTILWm84SY3SDNBa	t	2022-06-25	60	1
63	iwona1@gmail.com	$2y$10$8rZsR8gEGs8F0q1ol/gLEuBnCy3Wyv6.LauMnBziDNUb/Ll528qFm	t	2022-06-28	66	1
64	nieznajoma8@gmail.com	$2y$10$OL1w1Prosg5ANtkcISZuXex2jYds2.Up2wXOKrIZYZ7vOKIw/j2XK	t	2022-06-28	67	1
65	wojtek123@mail.com	$2y$10$UKqZZBqBMVnbB8UQrthqM.pe3SlfYa6ZB.JnXW8SgEWGi6P1pMtEG	t	2022-06-28	68	1
66	aga21@gmail.com	$2y$10$z40RIv8.JPi7cARb3pqzeutVYijsRcmF2IOzVsX6HGsA6TJ0bXW.i	t	2022-06-28	69	1
67	piotrek.k@gmail.com	$2y$10$UsTgB1Q3y/XsCYurwjTtkuLQIBGZ38YZXIwyuO1CoJfrDgSdgg0sq	t	2022-06-28	70	1
68	jerzy@mail.com	$2y$10$MvsdfZ3WhOWYbvlJ82v3HOLzYWr/C4sIFKf3syo.HOdQ58V17WZLy	t	2022-06-28	71	1
69	adamski99@gmail.com	$2y$10$Jela.eZgivgUj77IySSOweCGL1e9451y3s.U1D4d3W9wEYO0mIjn6	t	2022-06-28	72	1
70	eleo02@gmail.com	$2y$10$xkQlWwKF4ql2cO.Duc908OMqCzAHNEeXecnjS6efZKOssVJZ5gQmS	t	2022-06-28	73	1
\.


--
-- Data for Name: user_details; Type: TABLE DATA; Schema: public; Owner: bvckilovivwoay
--

COPY public.user_details (id, first_name, last_name, phone_number, address_id, image) FROM stdin;
1	Tom	Jones	123456789	1	tomjones.jpg
49	Ewelina	Drwal	123123123	34	man_face.jpg
61	Admin	Admin	123456789	69	man_face.jpg
62	Dominika	Frosztęga	123123123	70	face_womam_image.jpg
63	Ewelina	Drwal	123123123	71	fries.jpg
64	Ewelina	Drwal	123123123	72	face_womam_image.jpg
60	Damian	Herbut	555555555	66	cheese.jpg
65	Iwona	Zając	479497212	73	woman1.jpg
66	Iwona	Zając	479497212	74	woman1.jpg
67	Daniela	Kucharska	643745533	75	woman2.jpg
68	Wojciech	Kalinowski	312133645	76	man1.jpeg
69	Agnieszka	Kurowska	765678987	77	woman3.jpg
70	Piotr	Kowalczyk	667846937	78	man2.jpg
71	Jerzy	Milewski	847837837	79	man3.jpg
72	Michał	Adamski	984038394	80	man4.jpg
73	Eleonora	Sobczak	774883993	81	woman4.jpg
\.


--
-- Data for Name: user_favourite_stalls; Type: TABLE DATA; Schema: public; Owner: bvckilovivwoay
--

COPY public.user_favourite_stalls (id, user_id, stall_id) FROM stdin;
13	55	12
15	56	12
16	56	13
17	59	1
18	59	13
\.


--
-- Name: address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bvckilovivwoay
--

SELECT pg_catalog.setval('public.address_id_seq', 81, true);


--
-- Name: product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bvckilovivwoay
--

SELECT pg_catalog.setval('public.product_id_seq', 105, true);


--
-- Name: product_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bvckilovivwoay
--

SELECT pg_catalog.setval('public.product_type_id_seq', 1, true);


--
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bvckilovivwoay
--

SELECT pg_catalog.setval('public.role_id_seq', 2, true);


--
-- Name: stall_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bvckilovivwoay
--

SELECT pg_catalog.setval('public.stall_id_seq', 25, true);


--
-- Name: stall_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bvckilovivwoay
--

SELECT pg_catalog.setval('public.stall_type_id_seq', 35, true);


--
-- Name: user_details_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bvckilovivwoay
--

SELECT pg_catalog.setval('public.user_details_id_seq', 73, true);


--
-- Name: user_favourite_stalls_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bvckilovivwoay
--

SELECT pg_catalog.setval('public.user_favourite_stalls_id_seq', 19, true);


--
-- Name: user_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bvckilovivwoay
--

SELECT pg_catalog.setval('public.user_user_id_seq', 70, true);


--
-- Name: address address_pk; Type: CONSTRAINT; Schema: public; Owner: bvckilovivwoay
--

ALTER TABLE ONLY public.address
    ADD CONSTRAINT address_pk PRIMARY KEY (id);


--
-- Name: product product_pk; Type: CONSTRAINT; Schema: public; Owner: bvckilovivwoay
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_pk PRIMARY KEY (id);


--
-- Name: product_type product_type_pk; Type: CONSTRAINT; Schema: public; Owner: bvckilovivwoay
--

ALTER TABLE ONLY public.product_type
    ADD CONSTRAINT product_type_pk PRIMARY KEY (id);


--
-- Name: role role_pk; Type: CONSTRAINT; Schema: public; Owner: bvckilovivwoay
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_pk PRIMARY KEY (id);


--
-- Name: stall stall_pk; Type: CONSTRAINT; Schema: public; Owner: bvckilovivwoay
--

ALTER TABLE ONLY public.stall
    ADD CONSTRAINT stall_pk PRIMARY KEY (id);


--
-- Name: stall_type stall_type_pk; Type: CONSTRAINT; Schema: public; Owner: bvckilovivwoay
--

ALTER TABLE ONLY public.stall_type
    ADD CONSTRAINT stall_type_pk PRIMARY KEY (id);


--
-- Name: user_details user_details_pk; Type: CONSTRAINT; Schema: public; Owner: bvckilovivwoay
--

ALTER TABLE ONLY public.user_details
    ADD CONSTRAINT user_details_pk PRIMARY KEY (id);


--
-- Name: user_favourite_stalls user_favourite_stalls_pk; Type: CONSTRAINT; Schema: public; Owner: bvckilovivwoay
--

ALTER TABLE ONLY public.user_favourite_stalls
    ADD CONSTRAINT user_favourite_stalls_pk PRIMARY KEY (id);


--
-- Name: user user_pk; Type: CONSTRAINT; Schema: public; Owner: bvckilovivwoay
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pk PRIMARY KEY (id);


--
-- Name: address_id_uindex; Type: INDEX; Schema: public; Owner: bvckilovivwoay
--

CREATE UNIQUE INDEX address_id_uindex ON public.address USING btree (id);


--
-- Name: product_id_uindex; Type: INDEX; Schema: public; Owner: bvckilovivwoay
--

CREATE UNIQUE INDEX product_id_uindex ON public.product USING btree (id);


--
-- Name: product_type_id_uindex; Type: INDEX; Schema: public; Owner: bvckilovivwoay
--

CREATE UNIQUE INDEX product_type_id_uindex ON public.product_type USING btree (id);


--
-- Name: product_type_type_uindex; Type: INDEX; Schema: public; Owner: bvckilovivwoay
--

CREATE UNIQUE INDEX product_type_type_uindex ON public.product_type USING btree (type);


--
-- Name: role_id_uindex; Type: INDEX; Schema: public; Owner: bvckilovivwoay
--

CREATE UNIQUE INDEX role_id_uindex ON public.role USING btree (id);


--
-- Name: role_role_uindex; Type: INDEX; Schema: public; Owner: bvckilovivwoay
--

CREATE UNIQUE INDEX role_role_uindex ON public.role USING btree (role);


--
-- Name: stall_id_uindex; Type: INDEX; Schema: public; Owner: bvckilovivwoay
--

CREATE UNIQUE INDEX stall_id_uindex ON public.stall USING btree (id);


--
-- Name: stall_name_uindex; Type: INDEX; Schema: public; Owner: bvckilovivwoay
--

CREATE UNIQUE INDEX stall_name_uindex ON public.stall USING btree (name);


--
-- Name: stall_type_id_uindex; Type: INDEX; Schema: public; Owner: bvckilovivwoay
--

CREATE UNIQUE INDEX stall_type_id_uindex ON public.stall_type USING btree (id);


--
-- Name: stall_type_type_uindex; Type: INDEX; Schema: public; Owner: bvckilovivwoay
--

CREATE UNIQUE INDEX stall_type_type_uindex ON public.stall_type USING btree (type);


--
-- Name: user_details_id_uindex; Type: INDEX; Schema: public; Owner: bvckilovivwoay
--

CREATE UNIQUE INDEX user_details_id_uindex ON public.user_details USING btree (id);


--
-- Name: user_email_uindex; Type: INDEX; Schema: public; Owner: bvckilovivwoay
--

CREATE UNIQUE INDEX user_email_uindex ON public."user" USING btree (email);


--
-- Name: user_favourite_stalls_id_uindex; Type: INDEX; Schema: public; Owner: bvckilovivwoay
--

CREATE UNIQUE INDEX user_favourite_stalls_id_uindex ON public.user_favourite_stalls USING btree (id);


--
-- Name: user_user_id_uindex; Type: INDEX; Schema: public; Owner: bvckilovivwoay
--

CREATE UNIQUE INDEX user_user_id_uindex ON public."user" USING btree (id);


--
-- Name: product product_product_type_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: bvckilovivwoay
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_product_type_id_fk FOREIGN KEY (product_type_id) REFERENCES public.product_type(id);


--
-- Name: product product_stall_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: bvckilovivwoay
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_stall_id_fk FOREIGN KEY (stall_id) REFERENCES public.stall(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: stall_types_stall stall_types_stall___fk2; Type: FK CONSTRAINT; Schema: public; Owner: bvckilovivwoay
--

ALTER TABLE ONLY public.stall_types_stall
    ADD CONSTRAINT stall_types_stall___fk2 FOREIGN KEY (stall_type_id) REFERENCES public.stall_type(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: stall_types_stall stall_types_stall_stall_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: bvckilovivwoay
--

ALTER TABLE ONLY public.stall_types_stall
    ADD CONSTRAINT stall_types_stall_stall_id_fk FOREIGN KEY (stall_id) REFERENCES public.stall(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: stall stall_user_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: bvckilovivwoay
--

ALTER TABLE ONLY public.stall
    ADD CONSTRAINT stall_user_id_fk FOREIGN KEY (user_id) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user user_details___fk; Type: FK CONSTRAINT; Schema: public; Owner: bvckilovivwoay
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_details___fk FOREIGN KEY (user_details_id) REFERENCES public.user_details(id);


--
-- Name: user_details user_details_address_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: bvckilovivwoay
--

ALTER TABLE ONLY public.user_details
    ADD CONSTRAINT user_details_address_id_fk FOREIGN KEY (address_id) REFERENCES public.address(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user user_role___fk; Type: FK CONSTRAINT; Schema: public; Owner: bvckilovivwoay
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_role___fk FOREIGN KEY (role_id) REFERENCES public.role(id);


--
-- Name: DATABASE dfr0uql9sg1rnc; Type: ACL; Schema: -; Owner: bvckilovivwoay
--

REVOKE CONNECT,TEMPORARY ON DATABASE dfr0uql9sg1rnc FROM PUBLIC;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: bvckilovivwoay
--

REVOKE ALL ON SCHEMA public FROM postgres;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO bvckilovivwoay;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: LANGUAGE plpgsql; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON LANGUAGE plpgsql TO bvckilovivwoay;


--
-- PostgreSQL database dump complete
--

