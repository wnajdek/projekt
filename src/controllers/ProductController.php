<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/Product.php';
require_once __DIR__.'/../repository/ProductRepository.php';

class ProductController extends AppController {

    const MAX_FILE_SIZE = 1024*1024*1024*16;
    const SUPPORTED_TYPES = ['image/png', 'image/jpeg', 'image/gif'];
    const UPLOAD_DIRECTORY = '/../public/uploads/products/';

    private $productRepository;

    public function __construct()
    {
        parent::__construct();
        $this->productRepository = new ProductRepository();
    }

    public function addProduct() {
        $this->isLoggedIn();

        $stallId = $_SESSION['userStallId'];

        if ($this->isPost() && is_uploaded_file($_FILES['image']['tmp_name']) && $this->validate($_FILES['image'])) {
            if (!file_exists(dirname(__DIR__) . self::UPLOAD_DIRECTORY . $_SESSION['userStallId'])) {
                mkdir(dirname(__DIR__) . self::UPLOAD_DIRECTORY . $_SESSION['userStallId'], 0777, true);
            }
            move_uploaded_file(
                $_FILES['image']['tmp_name'],
                dirname(__DIR__) . self::UPLOAD_DIRECTORY . $_SESSION['userStallId'] . '/' . $_FILES['image']['name']
            );


            header('Content-Type: application/json');
            http_response_code(200);
            $product = new Product(
                $_POST['name'],
                $_POST['description'],
                $_POST['price'],
                $_FILES['image']['name'],
                1,
                $stallId
            );
            $productId = $this->productRepository->addProduct($product);

            $productFromDb = $this->productRepository->getProduct($productId);

            echo json_encode([
                'name'=>$productFromDb->getName(),
                'image'=>$productFromDb->getImage(),
                'description'=>$productFromDb->getDescription(),
                'price'=>$productFromDb->getPrice(),
                'id'=>$productFromDb->getId(),
                'stallId'=>$productFromDb->getStallId(),
                'productTypeId'=>$productFromDb->getProductTypeId()
            ]);
        }
    }

    public function updateProduct() {
        $this->isLoggedIn();

        $stallId = $_SESSION['userStallId'];


        if ($this->isPost()) {
            header('Content-Type: application/json');
            http_response_code(200);
            $image = null;
            if (is_uploaded_file($_FILES['image']['tmp_name'])  && $this->validate($_FILES['image'])) {
                if (!file_exists(dirname(__DIR__) . self::UPLOAD_DIRECTORY . $_SESSION['userStallId'])) {
                    mkdir(dirname(__DIR__) . self::UPLOAD_DIRECTORY . $_SESSION['userStallId'], 0777, true);
                }
                move_uploaded_file(
                    $_FILES['image']['tmp_name'],
                    dirname(__DIR__) . self::UPLOAD_DIRECTORY . $_SESSION['userStallId'] . '/' . $_FILES['image']['name']
                );

                $image = $_FILES['image']['name'];
            } else {
                $image = $this->productRepository->getProduct($_POST['id'])->getImage();  // save with old image
            }


            $product = new Product(
                $_POST['name'],
                $_POST['description'],
                $_POST['price'],
                $image,
                1,
                $stallId,
                $_POST['id']
            );
            $this->productRepository->updateProduct($product);

            $productFromDb = $this->productRepository->getProduct($_POST['id']);

            echo json_encode([
                'name'=>$productFromDb->getName(),
                'image'=>$productFromDb->getImage(),
                'description'=>$productFromDb->getDescription(),
                'price'=>$productFromDb->getPrice(),
                'id'=>$productFromDb->getId(),
                'stallId'=>$productFromDb->getStallId(),
                'productTypeId'=>$productFromDb->getProductTypeId()
            ]);

        }
    }

    public function deleteProduct() {
        $this->isLoggedIn();

        if ($this->isPost()) {
            header('Content-Type: application/json');
            http_response_code(200);

            $this->productRepository->deleteProduct($_POST['id']);

            echo json_encode(['id' => $_POST['id']]);
        }
    }


}